package com.training.ticketgenerator.mapper;

import com.training.ticketgenerator.builder.JsonControlBuilder;
import com.training.ticketgenerator.builder.JsonCreateTicketResponseBuilder;
import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketResponse;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class JsonCreateTicketResponseMapper {

  public JsonCreateTicketResponse mapSuccess(JsonCreateTicket jsonCreateTicket) {
    JsonControl jsonControl = JsonControlBuilder
        .builder()
        .result(ResponseStatus.SUCCESS)
        .build();
    return JsonCreateTicketResponseBuilder
        .builder()
        .control(jsonControl)
        .data(jsonCreateTicket)
        .build();
  }

  public JsonCreateTicketResponse mapError(Set<JsonIssue> jsonIssues) {

    JsonControl jsonControl = JsonControlBuilder
        .builder()
        .result(ResponseStatus.ERROR)
        .issues(jsonIssues)
        .build();
    return JsonCreateTicketResponseBuilder
        .builder()
        .control(jsonControl)
        .build();
  }
}
