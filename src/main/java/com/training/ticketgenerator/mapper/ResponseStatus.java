package com.training.ticketgenerator.mapper;

public class ResponseStatus {

  public static final String SUCCESS = "Success";
  public static final String ERROR = "Error";
}
