package com.training.ticketgenerator.mapper.responseentity;

import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;
import java.util.Set;

public class JsonCreateTicketErrorResponse {

  private JsonControl jsonControl;
  Set<JsonIssue> issues;

  public JsonControl getJsonControl() {
    return jsonControl;
  }

  public void setJsonControl(JsonControl jsonControl) {
    this.jsonControl = jsonControl;
  }

  public Set<JsonIssue> getIssues() {
    return issues;
  }

  public void setIssues(Set<JsonIssue> issues) {
    this.issues = issues;
  }
}
