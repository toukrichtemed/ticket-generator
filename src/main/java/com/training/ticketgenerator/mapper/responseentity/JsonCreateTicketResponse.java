package com.training.ticketgenerator.mapper.responseentity;

import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;

public class JsonCreateTicketResponse {

  private JsonControl control;
  private JsonCreateTicket data;

  public JsonControl getControl() {
    return control;
  }

  public void setControl(JsonControl control) {
    this.control = control;
  }

  public JsonCreateTicket getData() {
    return data;
  }

  public void setData(JsonCreateTicket data) {
    this.data = data;
  }
}
