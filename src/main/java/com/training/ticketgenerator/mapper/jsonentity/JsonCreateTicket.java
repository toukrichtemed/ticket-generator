package com.training.ticketgenerator.mapper.jsonentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.training.ticketgenerator.entities.QRCode;
import com.training.ticketgenerator.entities.User;
import java.util.Date;

public class JsonCreateTicket {

  private Long id;
  private String name;
  @JsonProperty("creation_date")
  private Date creationDate;
  private User user;

  @JsonProperty("qr_code")
  private QRCode QRCode;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public QRCode getQrCode() {
    return QRCode;
  }

  public void setQrCode(QRCode QRCode) {
    this.QRCode = QRCode;
  }
}
