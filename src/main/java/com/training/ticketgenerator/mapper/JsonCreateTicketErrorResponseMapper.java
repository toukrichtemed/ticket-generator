package com.training.ticketgenerator.mapper;

import com.training.ticketgenerator.builder.JsonControlBuilder;
import com.training.ticketgenerator.builder.JsonCreateTicketErrorResponseBuilder;
import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketErrorResponse;
import java.util.Set;
import org.springframework.stereotype.Component;

@Component
public class JsonCreateTicketErrorResponseMapper {

  public JsonCreateTicketErrorResponse map(Set<JsonIssue> jsonIssues) {

    JsonControl jsonControl = JsonControlBuilder
        .builder()
        .result(ResponseStatus.ERROR)
        .issues(jsonIssues)
        .build();
    return JsonCreateTicketErrorResponseBuilder
        .builder()
        .control(jsonControl)
        .issues(jsonIssues)
        .build();
  }
}
