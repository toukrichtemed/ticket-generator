package com.training.ticketgenerator.exceptions;

public class UserAlreadyExistException extends RuntimeException{

  public UserAlreadyExistException(String message) {
    super(message);
  }
}
