package com.training.ticketgenerator.builder;

import com.training.ticketgenerator.entities.QRCode;
import com.training.ticketgenerator.entities.User;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;

import java.util.Date;

public class JsonCreateTicketBuilder {

    private Long id;
    private String name;
    private Date creationDate;
    private User user;

    private QRCode QRCode;

    private JsonCreateTicketBuilder() {
    }

    public static JsonCreateTicketBuilder builder() {
        return new JsonCreateTicketBuilder();
    }

    public JsonCreateTicketBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public JsonCreateTicketBuilder name(String name) {
        this.name = name;
        return this;
    }

    public JsonCreateTicketBuilder creationDate(Date creationDate) {
        this.creationDate = creationDate;
        return this;
    }

    public JsonCreateTicketBuilder user(User user) {
        this.user = user;
        return this;
    }

    public JsonCreateTicketBuilder qrCode(QRCode QRCode) {
        this.QRCode = QRCode;
        return this;
    }

    public JsonCreateTicket build() {
        JsonCreateTicket jsonCreateTicket = new JsonCreateTicket();
        jsonCreateTicket.setId(id);
        jsonCreateTicket.setName(name);
        jsonCreateTicket.setCreationDate(creationDate);
        jsonCreateTicket.setUser(user);
        jsonCreateTicket.setQrCode(QRCode);

        return jsonCreateTicket;
    }
}
