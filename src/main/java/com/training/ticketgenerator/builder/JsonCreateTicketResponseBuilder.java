package com.training.ticketgenerator.builder;

import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketResponse;

public class JsonCreateTicketResponseBuilder {

  private JsonControl jsonControl;
  private JsonCreateTicket data;

  private JsonCreateTicketResponseBuilder(){
  }

  public static JsonCreateTicketResponseBuilder builder(){
    return new JsonCreateTicketResponseBuilder();
  }

  public JsonCreateTicketResponseBuilder control(JsonControl jsonControl){
    this.jsonControl = jsonControl;
    return this;
  }

  public JsonCreateTicketResponseBuilder data(JsonCreateTicket jsonCreateTicket){
    this.data = jsonCreateTicket;
    return this;
  }

  public JsonCreateTicketResponse build(){
    final JsonCreateTicketResponse response = new JsonCreateTicketResponse();
    response.setControl(jsonControl);
    response.setData(data);
    return response;
  }

}
