package com.training.ticketgenerator.builder;

import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.validation.ValidationIssueSeverity;

public class JsonIssueBuilder {

  private ValidationIssueSeverity severity;
  private String errorCode;
  private String message;

  private JsonIssueBuilder() {
  }

  public static JsonIssueBuilder builder() {
    return new JsonIssueBuilder();
  }

  public JsonIssueBuilder severity(ValidationIssueSeverity severity) {
    this.severity = severity;
    return this;
  }

  public JsonIssueBuilder errorCode(String errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public JsonIssueBuilder message(String message) {
    this.message = message;
    return this;
  }

  public JsonIssue build() {
    final JsonIssue jsonIssue = new JsonIssue();
    jsonIssue.setSeverity(severity);
    jsonIssue.setErrorCode(errorCode);
    jsonIssue.setMessage(message);
    return jsonIssue;
  }


}
