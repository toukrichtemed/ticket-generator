package com.training.ticketgenerator.builder;

import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import java.util.Set;

public class JsonControlBuilder {

  private String result;
  private Set<JsonIssue> jsonIssues;

  private JsonControlBuilder() {
  }

  public static JsonControlBuilder builder() {
    return new JsonControlBuilder();
  }

  public JsonControlBuilder result(String result) {
    this.result = result;
    return this;
  }

  public JsonControlBuilder issues(Set<JsonIssue> jsonIssues) {
    this.jsonIssues = jsonIssues;
    return this;
  }

  public JsonControl build() {
    final JsonControl jsonControl = new JsonControl();
    jsonControl.setResult(result);
    jsonControl.setJsonIssues(jsonIssues);
    return jsonControl;
  }

}
