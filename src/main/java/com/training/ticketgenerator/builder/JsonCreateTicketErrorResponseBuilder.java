package com.training.ticketgenerator.builder;

import com.training.ticketgenerator.builder.builderentity.JsonControl;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketErrorResponse;
import java.util.Set;

public class JsonCreateTicketErrorResponseBuilder {

  private JsonControl jsonControl;
  private Set<JsonIssue> issues;

  private JsonCreateTicketErrorResponseBuilder(){
  }

  public static JsonCreateTicketErrorResponseBuilder builder(){
    return new JsonCreateTicketErrorResponseBuilder();
  }

  public JsonCreateTicketErrorResponseBuilder control(JsonControl jsonControl){
    this.jsonControl = jsonControl;
    return this;
  }

  public JsonCreateTicketErrorResponseBuilder issues(Set<JsonIssue> issues){
    this.issues = issues;
    return this;
  }

  public JsonCreateTicketErrorResponse build(){
    final JsonCreateTicketErrorResponse response = new JsonCreateTicketErrorResponse();
    response.setJsonControl(jsonControl);
    response.setIssues(issues);
    return response;
  }

}
