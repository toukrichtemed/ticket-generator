package com.training.ticketgenerator.builder.builderentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

public class JsonControl {

  private String result;
  @JsonProperty("json_issues")
  private Set<JsonIssue>jsonIssues;

  public JsonControl() {
  }

  public JsonControl(String result, Set<JsonIssue> jsonIssues) {
    this.result = result;
    this.jsonIssues = jsonIssues;
  }

  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public Set<JsonIssue> getJsonIssues() {
    return jsonIssues;
  }

  public void setJsonIssues(Set<JsonIssue> jsonIssues) {
    this.jsonIssues = jsonIssues;
  }
}
