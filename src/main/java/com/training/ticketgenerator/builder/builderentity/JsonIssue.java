package com.training.ticketgenerator.builder.builderentity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.training.ticketgenerator.validation.ValidationIssueSeverity;

public class JsonIssue {

  private ValidationIssueSeverity severity;

  @JsonProperty("error_code")
  private String errorCode;
  private String message;

  public ValidationIssueSeverity getSeverity() {
    return severity;
  }

  public void setSeverity(ValidationIssueSeverity severity) {
    this.severity = severity;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
