package com.training.ticketgenerator.service;

import com.training.ticketgenerator.entities.User;
import com.training.ticketgenerator.exceptions.UserAlreadyExistException;
import com.training.ticketgenerator.repository.TicketRepository;
import com.training.ticketgenerator.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    private TicketRepository ticketRepository;

    public UserServiceImpl(
            final UserRepository userRepository,
            final TicketRepository ticketRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User createUser(User user) {
        final User dbUser = getUserByPhone(user.getPhone());
        if (dbUser != null) {
            throw new UserAlreadyExistException("User with phone " + dbUser.getPhone() + " already exists");
        }
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void updateUser(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserByPhone(String phone) {
        return userRepository.findByPhone(phone);
    }
}
