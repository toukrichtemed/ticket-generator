package com.training.ticketgenerator.service;

import com.training.ticketgenerator.entities.User;

import java.util.List;

public interface UserService {

    User createUser(User user);

    void deleteUser(Long id);

    void updateUser(User user);

    User getUserById(Long id);

    List<User> getAllUsers();

    User getUserByPhone(String phone);

}
