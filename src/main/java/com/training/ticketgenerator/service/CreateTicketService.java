package com.training.ticketgenerator.service;

import com.training.ticketgenerator.builder.JsonCreateTicketBuilder;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.entities.QRCode;
import com.training.ticketgenerator.entities.Ticket;
import com.training.ticketgenerator.entities.User;
import com.training.ticketgenerator.mapper.JsonCreateTicketResponseMapper;
import com.training.ticketgenerator.mapper.jsonentity.JsonCreateTicket;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketResponse;
import com.training.ticketgenerator.repository.QRCodeRepository;
import com.training.ticketgenerator.repository.TicketRepository;
import com.training.ticketgenerator.util.AbstractUtil;
import com.training.ticketgenerator.validation.ValidationIssuesBusinessRules;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class CreateTicketService extends AbstractUtil {

    private UserService userService;
    private QRCodeService qrCodeService;
    private TicketRepository ticketRepository;
    private QRCodeRepository qrCodeRepository;
    private JsonCreateTicketResponseMapper jsonCreateTicketResponseMapper;

    public CreateTicketService(
            final UserService userService,
            final TicketRepository ticketRepository,
            final QRCodeRepository qrCodeRepository,
            final QRCodeService qrCodeService,
            final JsonCreateTicketResponseMapper jsonCreateTicketResponseMapper) {

        this.userService = userService;
        this.ticketRepository = ticketRepository;
        this.qrCodeRepository = qrCodeRepository;
        this.qrCodeService = qrCodeService;
        this.jsonCreateTicketResponseMapper = jsonCreateTicketResponseMapper;
    }

    public JsonCreateTicketResponse createTicket(User user) throws IOException {
        Set<JsonIssue> jsonIssues = new HashSet<>();

        User dbUser;
        Resource resource;
        String name;

        try {
            dbUser = userService.createUser(user);
            name = "T" + dbUser.getPhone().substring(dbUser.getPhone().length() - 2) + "-" + dbUser.getName();
            String fileName = name.substring(0, 6);
            qrCodeService.generateQRCode(user.getQrContent(), fileName, 600, 600);
            resource = qrCodeService.loadQRCode(fileName);
        } catch (Exception e) {
            jsonIssues.add(getJsonUssue(ValidationIssuesBusinessRules.USER_ALREADY_EXIST));
            return jsonCreateTicketResponseMapper.mapError(jsonIssues);
        }
        Ticket dbTicket = saveTicket(resource, dbUser, name);

        JsonCreateTicket jsonCreateTicket =
                JsonCreateTicketBuilder
                        .builder()
                        .id(dbTicket.getId())
                        .name(dbTicket.getName())
                        .creationDate(dbTicket.getCreationDate())
                        .user(dbTicket.getUser())
                        .qrCode(dbTicket.getQrCode())
                        .build();

        return jsonCreateTicketResponseMapper.mapSuccess(jsonCreateTicket);
    }

    private Ticket saveTicket(Resource resource, User user, String name) throws IOException {
        Ticket ticket = new Ticket();
        ticket.setName(name);
        ticket.setCreationDate(new Date());
        ticket.setUser(user);
        QRCode qrCode = saveQrCode(resource);
        qrCodeRepository.save(qrCode);
        ticket.setQrCode(qrCode);
        return ticketRepository.save(ticket);
    }

    private QRCode saveQrCode(Resource resource) throws IOException {
        QRCode qrCode = new QRCode();
        qrCode.setFileName(resource.getFilename());
        qrCode.setFileType("image/png");
        qrCode.setFileUri(resource.getURI().toString());
        qrCode.setFileSize(resource.contentLength());
        return qrCode;
    }
}
