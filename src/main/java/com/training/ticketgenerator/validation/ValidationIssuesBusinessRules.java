package com.training.ticketgenerator.validation;

public enum ValidationIssuesBusinessRules {

  USER_ALREADY_EXIST(ValidationIssueSeverity.ERROR, "USER_ALREADY_EXIST",
      "User with specified phone already exists");

  private String message;
  private String errorCode;
  private ValidationIssueSeverity severity;

  ValidationIssuesBusinessRules(ValidationIssueSeverity severity, String errorCode,
      String message) {
    this.severity = severity;
    this.errorCode = errorCode;
    this.message = message;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public ValidationIssueSeverity getSeverity() {
    return severity;
  }

  public void setSeverity(ValidationIssueSeverity severity) {
    this.severity = severity;
  }
}
