package com.training.ticketgenerator.util;

import com.training.ticketgenerator.builder.JsonIssueBuilder;
import com.training.ticketgenerator.builder.builderentity.JsonIssue;
import com.training.ticketgenerator.validation.ValidationIssuesBusinessRules;

public abstract class AbstractUtil {

  protected JsonIssue getJsonUssue(ValidationIssuesBusinessRules validationIssuesBusinessRules) {
    return JsonIssueBuilder.builder().severity(validationIssuesBusinessRules.getSeverity())
        .errorCode(validationIssuesBusinessRules.getErrorCode())
        .message(validationIssuesBusinessRules.getMessage()).build();
  }
}
