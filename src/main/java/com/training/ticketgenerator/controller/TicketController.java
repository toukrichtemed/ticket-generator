package com.training.ticketgenerator.controller;

import com.training.ticketgenerator.entities.User;
import com.training.ticketgenerator.mapper.ResponseStatus;
import com.training.ticketgenerator.mapper.responseentity.JsonCreateTicketResponse;
import com.training.ticketgenerator.service.UserService;
import com.training.ticketgenerator.service.CreateTicketService;
import java.io.IOException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tickets")
public class TicketController {

  private CreateTicketService createTicketService;
  private UserService userService;

  public TicketController(final CreateTicketService createTicketService,
      final UserService userService) {
    this.createTicketService = createTicketService;
    this.userService = userService;
  }

  @PostMapping("/generate")
  public ResponseEntity<JsonCreateTicketResponse> generateTicket(@RequestBody User user)
      throws IOException {
    JsonCreateTicketResponse response = createTicketService.createTicket(user);
    if (response.getControl().getResult().equals(ResponseStatus.SUCCESS)) {
      return ResponseEntity.status(200).body(response);
    } else {
      return ResponseEntity.status(400).body(response);
    }
  }
}
